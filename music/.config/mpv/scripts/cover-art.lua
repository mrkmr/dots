local utils = require 'mp.utils'
local options = require 'mp.options'

COVERS = { 'cover.png', 'cover.jpg' }

function get_cover(dir)
    for _, cover in pairs(COVERS) do
        filepath = dir .. cover
        local res, err = utils.file_info(filepath)
        if res ~= nil then
            return filepath
        end
    end
    return nil
end

function loadCover()
    local path = mp.get_property('path', '')
    local dir, filename = utils.split_path(path)

    file_list = utils.readdir(dir, files)
    if string.find(filename, '.flac') then
        --cover = get_cover(dir)
        --if cover then
            -- mp.commandv("video-add", cover)
        --end
        os.execute("~/bin/album-art-wallpaper.sh &")
    end
end

mp.add_hook("on_load", 50, loadCover)
