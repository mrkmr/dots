set -o vi

export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
command -v ibus-daemon && ibus-daemon -drx

cred="\001$(tput     setaf 1  2>/dev/null)\002"
cgreen="\001$(tput   setaf 2  2>/dev/null)\002"
cyellow="\001$(tput  setaf 3  2>/dev/null)\002"
cblue="\001$(tput    setaf 12 2>/dev/null)\002"
cmagenta="\001$(tput setaf 13 2>/dev/null)\002"
creset="\001$(tput   sgr0     2>/dev/null)\002"

PROMPT_COMMAND=__prompt_command

__prompt_command() {
    local exit_status="$?"
    PS1=""

    # Print hostname if we are ssh'd in *and* we aren't in a tmux session.
    # Tmux session will already print the hostname so there's no need then.
    if [ -n "${SSH_CONNECTION}" ] && [ -z "${TMUX}" ]; then
        PS1+="\h "
    fi

    # Print venv if we are in a python virtual environment.
    if [ -n "${VIRTUAL_ENV}" ]; then
        PS1+="venv "
    fi

    # Print the basename of the CWD
    PS1+="${cblue}\W${creset} "

    # Print the last command's exit code. Colored based on the exit code.
    local coloring="${cgreen}"
    if [ "${exit_status}" -ne 0 ]; then
        coloring="${cred}"
    fi
    PS1+="${coloring}${exit_status}${creset} "

    # Print the actual shell prompt
    PS1+="${creset}->${creset} "
}

export GOPATH=~/.go
export PATH=~/bin:$PATH:~/.go/bin:~/.cargo/bin:~/.local/bin
export PATH=$PATH:~/hack/ext/google-cloud-sdk/bin
#/usr/lib/ccache/bin
export PATH=/usr/local/texlive/2020/bin/x86_64-linux/:$PATH

export FZF_DEFAULT_COMMAND='ag --ignore "*.o" -g ""'
export VISUAL=vim
export EDITOR="$VISUAL"

# command/program shortcuts
alias c=clear
alias e="${EDITOR}"
ef() {
    "${EDITOR}" "$(fzf)"
}
alias o=mimeopen
alias cp="cp --reflink=auto"
alias ls="ls --color=auto"
alias l="ls -lh"
alias la="ls -alh"
alias p="readlink -f"
alias xc="xsel -b"
alias swgl="sudo watch -c genlop -ci"
alias pr="pulseaudio --kill; pulseaudio --start"
alias pm=pulsemixer
alias btl=bluetoothctl
alias play="mpv --no-vid"
alias picomr="pkill -u ${USER} picom; picom --backend glx -f -D 5 --vsync -b"
alias upkill="pkill -u ${USER}"
alias upgrep="pgrep -u ${USER}"
alias info="info --vi-keys"
alias bcl="bc -l"

ninc() {
    ninja -C build
}

manr() {
    rst2man.py "${1}" | man -l -
}

alias lg="lynx https://google.com"

rin() {
    rsync --ignore-existing -rin $1 $2
    rsync --ignore-existing -rin $2 $1
}

mysotime() {
    date +%Y.%m.%dT%H:%M:%S
}

mcd() {
    mkdir -p "${1}"
    cd "${1}"
}

j() {
    res="$(~/bin/jump_helper.py "$1")"
    if [ "$?" -ne "1" ]; then
        echo "Jumping to $res"
        cd "${res}"
        ls
    else
        # Print available jumps from script output
        echo "$res"
        return 1
    fi
}

# config shortcuts
alias ebrc="${EDITOR} ~/.bashrc"
alias dotbrc=". ~/.bashrc"
alias etmux="${EDITOR} ~/.tmux.conf"
alias evi="${EDITOR} ~/.vimrc"
alias eob="${EDITOR} ~/.config/openbox/rc.xml"
alias eoba="${EDITOR} ~/.config/openbox/autostart.sh"
alias epnl="${EDITOR} ~/.config/openbox/panel.sh"
alias exr="${EDITOR} ~/.Xresources"
alias dotxr="xrdb ~/.Xresources"

# disk status
alias syncstatus="watch grep -e Dirty: -e Writeback: /proc/meminfo"

# networking
alias externalip="wget -qO- http://ipecho.net/plain ; echo"
alias nettest="ping -c 1 google.com"

# tmux
alias tmuxn="tmux new-session -s"
alias tls='tmux ls'
tat() { tmux attach -t $1; }

# GPG won't be able to invoke pinentry without this.
export GPG_TTY="$(tty)"

dur() {
    start="$(date +%s)"
    $@
    end="$(date +%s)"
    diff="$(echo "${end} - ${start}" | bc)"
    minutes="$(echo "${diff} / 60" | bc)"
    seconds="$(echo "${diff} % 60" | bc)"
    echo "Took ${minutes} minutes, ${seconds} seconds."
}

tto() {
    $@ 2>&1 | less
}

# git
alias ga="git add"
alias gan="git add -N"
alias gcam="git commit -am"
alias gcamen="git commit --amend"
alias gc="git commit"
alias gcm="git commit -m"
alias gco="git checkout"
alias gd="git diff"
alias gl="git pull"
alias glg="git log"
alias glgp="git log -p"
alias glgs="git log --show-signature"
alias gp="git push"
alias gs="git status"

# history
HISTSIZE=10240
HISTFILE=~/.history

export HISTSIZE HISTFILE
CARGO_ENV="$HOME/.cargo/env"
[ -f "${CARGO_ENV}" ] && . "${CARGO_ENV}"
. "$HOME/.cargo/env"
