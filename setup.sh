#!/bin/sh

set -e
set -x

mkdir -m 0700 -p ~/.gnupg

rm -f ~/.config/user-dirs.dirs

stow music
stow vim
stow openbox
stow shell

mkdir -p ~/.vim/bak
mkdir -p ~/.vim/swp
mkdir -p ~/.vim/und
