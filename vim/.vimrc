call plug#begin('~/.local/share/nvim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'preservim/tagbar'
Plug 'puremourning/vimspector'
Plug 'rust-lang/rust.vim'
call plug#end()

colorscheme default
let g:lightline = {
            \ 'colorscheme': 'jellybeans',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ],
            \             [ 'readonly', 'filename', 'modified' ] ]
            \ },
            \ }

let g:vimspector_enable_mappings = 'HUMAN'

set wildmenu
set wildmode=list:longest

" set pwd to new file location
" set autochdir

" persistent undo
set backup
set undofile
set directory=~/.vim/swp//
set undodir=~/.vim/und//
set backupdir=~/.vim/bak//

" save last position
autocmd BufReadPost * if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif

filetype plugin indent on
syntax enable

set list
set listchars=eol:¬,tab:›―

set nu
set rnu
" set cursorcolumn
" set cursorline
" highlight cursorcolumn ctermbg=210 cterm=none
" highlight cursorline   ctermbg=210 cterm=none
let g:netrw_bufsettings = 'noma nomod rnu nobl nowrap ro'

" case insensitive
set ignorecase
set smartcase

" center searches
nnoremap n nzz
nnoremap N Nzz

" uber speed
let mapleader = " "
map ; :
noremap <Leader>s :sort<CR>
nnoremap <Leader>w :write<CR>
nnoremap <Leader>q :quit<CR>
nnoremap <Leader>x :x<CR>
nnoremap <Leader>h :noh<CR>
nnoremap <Leader>e :Explore<CR>
nnoremap <Leader>f :FZF<CR>
nnoremap <Leader>F :tabe<CR>:FZF<CR>

" ctags
nnoremap <Leader>d <C-w><C-]><C-w>T
nnoremap <Leader>] <C-]>
nnoremap <Leader>t <C-t>

" search selected
vnoremap // y/<C-R>"<CR>

" fzf commands
map ' :Files<CR>
nnoremap <Leader>b :Buffers<CR>

" vertical help
cnoremap vh vert help 

" tabs
nnoremap <C-n> :tabnew<CR>
nnoremap J :tabprevious<CR>
nnoremap K :tabnext<CR>
nnoremap T :tabe 

" window manipulation
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>
nnoremap <A-=> <C-w>=

" compile shortcuts
nnoremap <Leader>c :write<CR>:!make<CR><CR>
nnoremap <Leader>g :write<CR>:!sh -c "pdflatex %; pkill -HUP mupdf"<CR>

" how wide?
set textwidth=80
set colorcolumn=81

" tab/spaces
set expandtab
set shiftwidth=4
set tabstop=4

autocmd FileType cpp        setlocal shiftwidth=2 tabstop=2
autocmd FileType css        setlocal shiftwidth=2 tabstop=2
autocmd FileType go         setlocal shiftwidth=4 tabstop=4 noexpandtab
autocmd FileType java       setlocal shiftwidth=4 tabstop=4
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType make       setlocal shiftwidth=8 tabstop=8 noexpandtab
autocmd FileType tex        setlocal noautoindent nosmartindent indentexpr=""
autocmd FileType typescript setlocal shiftwidth=2 tabstop=2
autocmd FileType xml        setlocal shiftwidth=2 tabstop=2
