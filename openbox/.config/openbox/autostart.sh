#!/bin/sh

[ -z "$(pgrep -u "${USER}" redshift)" ] && redshift -l "$(< ~/.location)" &

if [ -z "$(pgrep -u "${USER}" gnome-keyring)" ]; then
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
fi

pkill -u "${USER}" xss-lock
XSECURELOCK_SHOW_DATETIME=1                  \
    XSECURELOCK_PASSWORD_PROMPT=cursor       \
    XSECURELOCK_FONT="xos4 Terminus:size=14" \
    xss-lock -l -- xsecurelock &

[ -z "$(pgrep -u "${USER}" unclutter)" ] && unclutter &

pkill -u "${USER}" picom
picom \
    --backend glx --vsync \
    --fading --fade-delta 3 \
    --shadow --no-dock-shadow \
    --daemon

xrdb ~/.Xresources

xinput set-prop "SynPS/2 Synaptics TouchPad" "libinput Tapping Enabled" 1
xinput set-prop "SynPS/2 Synaptics TouchPad" "libinput Accel Speed"     0.37

#if [ -z "$(pgrep -u "${USER}" xwrits)" ]; then
#    xwrits +mono t=5  b=0.1 i=3  +auto-dismiss +beep +c +bc & # micropause
#    xwrits       t=30 b=3   i=13 +beep +c +bc & # take a (big) break from typing
#fi

#[ -z "$(pgrep -u "${USER}" oneko)" ] && oneko -tomoyo &

# start panel only after displays have settled - so the panel goes to the
# primary monitor when doing dual displays.
autorandr --change
feh --bg-fill ~/mine/configs/walls/current.png
pkill -u "${USER}" panel.sh; ~/.config/openbox/panel.sh &
#pkill -u "${USER}" polybar; polybar mybar
