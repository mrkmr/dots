#!/bin/sh

foreground="#f3f4f5"
background="#dd232c43"
color_bad="#ff7796"
color_warn="#fb8a3b"
color_good="#1abc9c"
color_info="#82aad7"

off_color="#afacaa"
r_separator="%{F${off_color}}>%{F${foreground}}"
l_separator="%{F${off_color}}<%{F${foreground}}"

battery_colors() {
    BAT_STATUS="$1"
    BAT_PERCENT="$2"
    if [ "$BAT_STATUS" = Charging ]; then
        echo -n "%{F${color_good}}"
    elif [ "$BAT_PERCENT" -gt 30 ]; then
        echo -n "%{F${foreground}}"
    else
        if [ "$BAT_PERCENT" -gt 20 ]; then
            echo -n "%{F${color_warn}}"
        else
            echo -n "%{F${color_bad}}"
        fi
    fi
    echo -n " ${BAT_STATUS} ${BAT_PERCENT}"
}

# gives output if connected, otherwise nothing
is_bluetooth_connected() {
    bluetoothctl devices | while read _ mac _; do
    connected="$(bluetoothctl info "${mac}" | \
        grep "Connected: yes")"
            if [ -n "${connected}" ]; then
                echo true
            fi
        done
}

left_info() {
    echo -n "%{B${background}}%{F${foreground}}"
    echo -n "%{l}" # to the left
    if [ -d /sys/class/power_supply/BAT0 ]; then
        BAT_STATUS="$(cat /sys/class/power_supply/BAT0/status)"
        BAT_PERCENT="$(cat /sys/class/power_supply/BAT0/capacity)"
        battery_colors "$BAT_STATUS" "$BAT_PERCENT"

        if [ -d /sys/class/power_supply/BAT1 ]; then
            BAT_STATUS="$(cat /sys/class/power_supply/BAT1/status)"
            BAT_PERCENT="$(cat /sys/class/power_supply/BAT1/capacity)"
            echo -n " %{F${off_color}}|%{F-}"
            battery_colors "$BAT_STATUS" "$BAT_PERCENT"
        fi

        echo -n "%{F${foreground}}"
        echo -n " $r_separator"
    fi

    bluetooth_status="$(rfkill list bluetooth | \
        awk '/Soft/{print $3; exit}')"
    if [ "${bluetooth_status}" = "no" ]; then
        # bluetoothd daemon may be not up which makes bluetoothctl hang.
        if [ -z "$(pgrep bluetoothd)" ]; then
            echo -n " %{F${color_bad}}bluetoothd not up%{F${foreground}}"
        else
            echo -ne " %{F${off_color}}BL %{F${foreground}}"

            is_powered="$(bluetoothctl show | awk '/Powered:/{print $2}')"
            if [ "${is_powered}" = "no" ]; then
                status="off"
            else
                is_conn="$(is_bluetooth_connected)"
                if [ -n "${is_conn}" ]; then
                    status="connected"
                else
                    status="on"
                fi
            fi
        fi
        echo -n "${status} ${r_separator}"
    fi

    # immanuel doesn't use wifi - always plugged into ethernet.
    if [ "$(hostname)" != "immanuel" ]; then
        if [ -n "$(rfkill list | grep -A 1 -w Wireless | grep yes)" ]; then
            echo -n "%{F#c0392b}"
            echo -n " airplane"
            echo -n "%{F${foreground}}"
        else
            wifi_status="$(iwctl station wlan0 show)"

            # either 'connected' or 'disconnected'
            connect_state="$(echo "${wifi_status}" | awk '/State/{print $2}')"

            if [ "${connect_state}" = "disconnected" ]; then
                echo -n " aloof"
            else
                network="$(echo "${wifi_status}" | \
                    awk '/Connected network/{print $3}')"

                if [ "$(echo "$network" | wc -m)" -gt "19" ]; then
                    echo -n " $(echo $network | cut -b-15)..."
                else
                    echo -n " $network"
                fi
            fi
        fi

        echo -n " ${r_separator}"
    fi

    if [ "$(pulsemixer --get-mute)" = "1" ]; then
        echo -n "%{F${color_info}}"
        echo -n " MUTE "
    else
        echo -n "%{F${color_warn}}"
        echo -n " $(pulsemixer --get-volume | awk '{print $1}') "
    fi

    micprops="$(pulsemixer --list-sources | grep Default$ | sed 's/, /\n/g')"
    micmute="$(echo "${micprops}" | grep ^Mute | awk -F": " '{print $2}')"
    echo -n "${r_separator}%{F${color_info}} "
    if [ "$micmute" -eq 1 ]; then
        echo -n "%{F${color_info}}MICMUTE "
    else
        micvol="$(echo "${micprops}" | grep ^Volumes)"
        echo -n "MIC %{F${color_warn}}$(echo "${micvol}" | \
            grep -Po "\d+") "
    fi

    # check if the VM is suspended, in which case the user is not notified
    if [ "$(pgrep qemu)" ]; then
        if [ "$(ps -ho stat "$(pgrep qemu)" | grep -v T)" ]; then
            echo -n "${r_separator}%{F${color_info}} "
            echo -n "VM running"
            echo -n "%{F${foreground}} "
        fi
    fi

    echo -n "%{B-}%{F-}" # clear colors
}

workspaces() {
    echo -n "%{c}" #center

    # Desktops are zero-indexed.
    current="$(xdotool get_desktop)"
    number="$(echo "$(xdotool get_num_desktops) - 1" | bc)"

    for desktop in $(seq 0 "${number}"); do
        if [ "${desktop}" = "${current}" ]; then
            # Fully white for current desktop.
            echo -n "%{F${background}}%{B${foreground}}"
        else
            # Black bg with white fg for inactive desktops.
            echo -n "%{B${background}}%{F${foreground}}"
        fi
        # if has windows, use filled
        if [ "$(xdotool search --desktop "$desktop" --name '' | \
            wc -l)" != 0 ]; then
            glyph="|"
        else
            glyph="o"
        fi
        echo -n " ${glyph} "
    done
    echo -n "%{B-}%{F-}" # clear colors
}

title() {
    current_window_title="$(xdotool getactivewindow getwindowname)"

    if [ "$(echo "${current_window_title}" | wc -m)" -gt "102" ]; then
        current_window_title="$(echo $current_window_title | cut -b-99)..."
    elif [ -z "${current_window_title}" ]; then
        current_window_title="none"
    fi

    # spacing between workspace and title
    echo -n "    "

    echo -n "%{F${foreground}}%{B${background}} ${current_window_title} "
    echo -n "%{B-}%{F-}" # clear colors
}

right_info() {
    echo -n "%{B${background}}%{F${foreground}}"
    echo -n "%{r}" # to the right

    # warn if there is something in the clipboard
    if [ "$(xsel -b | wc -m)" -ne 0 ]; then
        echo -n "%{F${color_warn}} CLIP%{F${foreground}} ${l_separator}"
    fi

    # something playing?
    music_status="$(playerctl status -a | grep -E "Playing|Paused")"
    if [ -n "${music_status}" ]; then
        # Grab the first playing - default list is in first come order so even
        # if the first player is paused and the second is playing, it'll show
        # first player.
        player="$(playerctl -l | sed -n 1p)"
        for p in $(playerctl -l); do
            if [ -n "$(playerctl -p "${p}" status | grep -F Playing)" ]; then
                player="${p}"
                break
            fi
        done

        # put play or pause icon
        if [ "${music_status}" == "Playing" ]; then
            echo -n "%{F${off_color}} playing"
        else
            echo -n "%{F${off_color}} paused"
        fi

        music_info="$(playerctl -p "${player}" \
            metadata --format '{{title}} by {{artist}}')"

        echo -n "%{F${foreground}} "
        if [ "$(echo "${music_info}" | wc -m)" -gt "60" ]; then
            echo -n "$(echo "${music_info}" | cut -b-55)..."
        else
            echo -n "${music_info}"
        fi
        echo -n "%{F${foreground}} "
        echo -n "${l_separator}"
    fi

    # load averages, just for the past minute
    curload="$(awk '{print $1}' /proc/loadavg)"
    echo -n "%{F${color_info}} load"
    echo -n "%{F${foreground}} ${curload}"

    # memory usage
    memory="$(free --si -h | awk '/^Mem:/{print $7}')"
    echo -n " ${l_separator}%{F${color_info}} free mem"
    echo -n "%{F${foreground}} ${memory}"

    # date and time
    echo -n " ${l_separator} $(date +'%I:%M %p')"
    echo -n " ${l_separator} $(date +'%a %b %-d')"

    echo -n " %{F${off_color}}| %{F${color_info}}$(whoami)"

    echo -n " %{B-}%{F-}" # clear colors
}

monitor_resolution="$(xrandr | awk '/primary/{print $4}')"
# if no monitor is marked as primary, use the first connected
[ -z "${monitor_resolution}" ] && \
    monitor_resolution="$(xrandr | awk '/connected/{print $3; exit}')"
bar_width="$(echo "$monitor_resolution" | awk -Fx '{print ($1 - 40)}')"
monitor_offset=$(echo "${monitor_resolution}" | awk -F+ '{print $2}')
offset_x="$(($monitor_offset + 20))"

while true; do
    left_info
    workspaces
    title
    right_info
    echo "" # flush with newline
    sleep 2
done | lemonbar           \
    -B  "#00000000"          \
    -g "${bar_width}x25+${offset_x}+10"
    #-f "-misc-fixed-medium-r-normal--20-140-100-100-c-100-iso8859-1" \
